# Nativescript-Pushwoosh

Nativescript plug-in for integration with Pushwoosh native Android and iOS SDK


## Installation

```bash
npm i --save nativescript-pushwoosh
```

## Configurations
In order to get it work remember:

* Configure your app AndroidManifest.xml and Info.plist with Push notifications settings enabled and Pushwoosh service configurations
* Android, copy the google-services.json under ./App_Resources/Android/
* OS, copy your <app-name>-entitlements.plist under ./App_Resources/iOS/ and specify the option CODE_SIGN_ENTITLEMENTS = <appname>/Resources/<appname>.entitlements into your build build.xcconfig

## Usage

Into your main.ts

#### iOS
```js
import { Pushwoosh } from "nativescript-pushwoosh";

if (application.ios) {
    application.ios.delegate = Pushwoosh.getInstance();
}
```

#### Android
```js
import { Pushwoosh } from "nativescript-pushwoosh";

if (application.android) {
    Pushwoosh.getInstance().registerForPushNotifications();
}
```

