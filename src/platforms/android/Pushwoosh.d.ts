/// <reference path="../../node_modules/tns-platform-declarations/android.d.ts" />

declare module android {
    export module support {
        export module v4 {
            export module app {
                export abstract class FixedPushwooshJobIntentService {
                    public static class: java.lang.Class<android.support.v4.app.FixedPushwooshJobIntentService>;
                    public onCreate(): void;
                    public constructor();
                }
            }
        }
    }
}

declare module android {
    export module support {
        export module v4 {
            export module app {
                export class FixedPushwooshJobServiceEngineImpl {
                    public static class: java.lang.Class<android.support.v4.app.FixedPushwooshJobServiceEngineImpl>;
                    public onStartJob(param0: any): boolean;
                    public compatGetBinder(): android.os.IBinder;
                    public dequeueWork(): any;
                    public onStopJob(param0: any): boolean;
                }
                export module FixedPushwooshJobServiceEngineImpl {
                    export class a {
                        public static class: java.lang.Class<android.support.v4.app.FixedPushwooshJobServiceEngineImpl.a>;
                        public complete(): void;
                        public getIntent(): android.content.Intent;
                    }
                }
            }
        }
    }
}

declare module com {
    export module google {
        export module firebase {
            export module messaging {
                export class RemoteMessage {
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module event {
                export class Event {
                    public static class: java.lang.Class<com.pushwoosh.internal.event.Event>;
                    public constructor(implementation: {
                    });
                    public constructor();
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export class BootReceiver {
            public static class: java.lang.Class<com.pushwoosh.BootReceiver>;
            public onReceive(param0: globalAndroid.content.Context, param1: globalAndroid.content.Intent): void;
            public constructor();
        }
        export module BootReceiver {
            export class DeviceBootedEvent extends com.pushwoosh.internal.event.Event {
                public static class: java.lang.Class<com.pushwoosh.BootReceiver.DeviceBootedEvent>;
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export class DeepLinkActivity extends com.pushwoosh.internal.utils.TranslucentActivity {
            public static class: java.lang.Class<com.pushwoosh.DeepLinkActivity>;
            public onCreate(param0: globalAndroid.os.Bundle): void;
            public constructor();
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module utils {
                export class TranslucentActivity {
                    public static class: java.lang.Class<com.pushwoosh.internal.utils.TranslucentActivity>;
                    public onCreate(param0: globalAndroid.os.Bundle): void;
                    public constructor();
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export class FcmRegistrationService {
            public static class: java.lang.Class<com.pushwoosh.FcmRegistrationService>;
            public onTokenRefresh(): void;
            public constructor();
        }
    }
}

declare module com {
    export module pushwoosh {
        export class GDPRManager {
            public static class: java.lang.Class<com.pushwoosh.GDPRManager>;
            public static TAG: string;
            public showGDPRDeletionUI(): void;
            public showGDPRConsentUI(): void;
            public static getInstance(): com.pushwoosh.GDPRManager;
            public isCommunicationEnabled(): boolean;
            public setCommunicationEnabled(param0: boolean, param1: com.pushwoosh.function1.Callback<java.lang.Void, com.pushwoosh.exception.PushwooshException>): void;
            public removeAllDeviceData(param0: com.pushwoosh.function1.Callback<java.lang.Void, com.pushwoosh.exception.PushwooshException>): void;
            public isDeviceDataRemoved(): boolean;
            public isAvailable(): boolean;
        }
    }
}

declare module com {
    export module pushwoosh {
        export class NetworkReceiver {
            public static class: java.lang.Class<com.pushwoosh.NetworkReceiver>;
            public onReceive(param0: globalAndroid.content.Context, param1: globalAndroid.content.Intent): void;
            public constructor();
        }
    }
}

declare module com {
    export module pushwoosh {
        export class NotificationOpenReceiver {
            public static class: java.lang.Class<com.pushwoosh.NotificationOpenReceiver>;
            public onReceive(param0: globalAndroid.content.Context, param1: globalAndroid.content.Intent): void;
            public constructor();
        }
    }
}

declare module com {
    export module pushwoosh {
        export class PluginAPI {
            public static class: java.lang.Class<com.pushwoosh.PluginAPI>;
            public static handleTokenRefresh(): void;
        }
    }
}

declare module com {
    export module pushwoosh {
        export class PushFcmIntentService {
            public static class: java.lang.Class<com.pushwoosh.PushFcmIntentService>;
            public constructor();
            public onMessageReceived(param0: any): void;
        }
    }
}

declare module com {
    export module pushwoosh {
        export module exception {
            export class PushwooshException {
                public static class: java.lang.Class<com.pushwoosh.exception.PushwooshException>;
                public constructor(param0: string, param1: java.lang.Throwable);
                public constructor(param0: java.lang.Throwable);
                public constructor(param0: string);
            }

            export class RegisterForPushNotificationsException extends com.pushwoosh.exception.PushwooshException {
                public static class: java.lang.Class<com.pushwoosh.exception.RegisterForPushNotificationsException>;
                public constructor(param0: string, param1: java.lang.Throwable);
                public constructor(param0: java.lang.Throwable);
                public constructor(param0: string);
            }

            export class UnregisterForPushNotificationException extends com.pushwoosh.exception.PushwooshException {
                public static class: java.lang.Class<com.pushwoosh.exception.UnregisterForPushNotificationException>;
                public constructor(param0: string, param1: java.lang.Throwable);
                public constructor(param0: java.lang.Throwable);
                public constructor(param0: string);
            }

            export class GetTagsException extends com.pushwoosh.exception.PushwooshException {
                public static class: java.lang.Class<com.pushwoosh.exception.GetTagsException>;
                public constructor(param0: string, param1: java.lang.Throwable);
                public constructor(param0: java.lang.Throwable);
                public constructor(param0: string);
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module function1 {
            export class Callback<T, E> extends java.lang.Object {
                public static class: java.lang.Class<com.pushwoosh.function1.Callback<any, any>>;
                public constructor(implementation: {
                    process(param0: com.pushwoosh.function1.Result<T, E>): void;
                });
                public constructor();
                public process(param0: com.pushwoosh.function1.Result<T, E>): void;
            }
            export class Result<T, E> extends java.lang.Object {
                public static class: java.lang.Class<com.pushwoosh.function1.Result<any, any>>;
                public static fromData(param0: any): com.pushwoosh.function1.Result<any, any>;
                public isSuccess(): boolean;
                public getException(): E;
                public static from(param0: any, param1: com.pushwoosh.exception.PushwooshException): com.pushwoosh.function1.Result<any, any>;
                public static fromException(param0: com.pushwoosh.exception.PushwooshException): com.pushwoosh.function1.Result<any, any>;
                public getData(): T;
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export class Pushwoosh {
            public static class: java.lang.Class<com.pushwoosh.Pushwoosh>;
            public static PUSH_RECEIVE_EVENT: string;
            public static PUSH_HISTORY_CAPACITY: number;
            public getHwid(): string;
            public static getInstance(): com.pushwoosh.Pushwoosh;
            public getTags(param0: com.pushwoosh.function1.Callback<com.pushwoosh.tags.TagsBundle, com.pushwoosh.exception.GetTagsException>): void;
            public clearLaunchNotification(): void;
            public getAppId(): string;
            public registerForPushNotifications(callback: (param0: string, exception: RegisterForPushNotificationsException) => void): void;
            public getLanguage(): string;
            public unregisterForPushNotifications(): void;
            public unregisterForPushNotifications(param0: com.pushwoosh.function1.Callback<string, com.pushwoosh.exception.UnregisterForPushNotificationException>): void;
            public sendTags(param0: com.pushwoosh.tags.TagsBundle, param1: com.pushwoosh.function1.Callback<java.lang.Void, com.pushwoosh.exception.PushwooshException>): void;
            public sendAppOpen(): void;
            public scheduleLocalNotification(param0: com.pushwoosh.notification.LocalNotification): com.pushwoosh.notification.LocalNotificationRequest;
            public getPushHistory(): java.util.List<com.pushwoosh.notification.PushMessage>;
            public setLanguage(param0: string): void;
            public sendTags(param0: com.pushwoosh.tags.TagsBundle): void;
            public setAppId(param0: string): void;
            public getPushToken(): string;
            public registerForPushNotifications(): void;
            public getLaunchNotification(): com.pushwoosh.notification.PushMessage;
            public sendInappPurchase(param0: string, param1: java.math.BigDecimal, param2: string): void;
            public clearPushHistory(): void;
            public setSenderId(param0: string): void;
            public getSenderId(): string;
        }
    }
}

declare module com {
    export module pushwoosh {
        export class PushwooshFcmHelper {
            public static class: java.lang.Class<com.pushwoosh.PushwooshFcmHelper>;
            public static isPushwooshMessage(param0: com.google.firebase.messaging.RemoteMessage): boolean;
            public static onMessageReceived(param0: globalAndroid.content.Context, param1: com.google.firebase.messaging.RemoteMessage): boolean;
            public static onTokenRefresh(param0: globalAndroid.content.Context, param1: string): void;
            public constructor();
        }
    }
}

declare module com {
    export module pushwoosh {
        export class PushwooshInitProvider {
            public static class: java.lang.Class<com.pushwoosh.PushwooshInitProvider>;
            public onCreate(): boolean;
            public update(param0: globalAndroid.net.Uri, param1: globalAndroid.content.ContentValues, param2: string, param3: native.Array<string>): number;
            public delete(param0: globalAndroid.net.Uri, param1: string, param2: native.Array<string>): number;
            public constructor();
            public query(param0: globalAndroid.net.Uri, param1: native.Array<string>, param2: string, param3: native.Array<string>, param4: string): globalAndroid.database.Cursor;
            public insert(param0: globalAndroid.net.Uri, param1: globalAndroid.content.ContentValues): globalAndroid.net.Uri;
            public getType(param0: globalAndroid.net.Uri): string;
        }
    }
}

declare module com {
    export module pushwoosh {
        export class PushwooshService extends com.pushwoosh.internal.platform.service.PushwooshJobService {
            public static class: java.lang.Class<com.pushwoosh.PushwooshService>;
            public onHandleWork(param0: globalAndroid.content.Intent): void;
            public constructor();
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module prefs {
                    export class PrefsProvider {
                        public static class: java.lang.Class<com.pushwoosh.internal.platform.prefs.PrefsProvider>;
                        public constructor(implementation: {
                            providePrefs(param0: string): globalAndroid.content.SharedPreferences;
                            provideDefault(): globalAndroid.content.SharedPreferences;
                        });
                        public constructor();
                        public providePrefs(param0: string): globalAndroid.content.SharedPreferences;
                        public provideDefault(): globalAndroid.content.SharedPreferences;
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export class PushwooshSharedDataProvider {
            public static class: java.lang.Class<com.pushwoosh.PushwooshSharedDataProvider>;
            public onCreate(): boolean;
            public update(param0: globalAndroid.net.Uri, param1: globalAndroid.content.ContentValues, param2: string, param3: native.Array<string>): number;
            public delete(param0: globalAndroid.net.Uri, param1: string, param2: native.Array<string>): number;
            public constructor();
            public query(param0: globalAndroid.net.Uri, param1: native.Array<string>, param2: string, param3: native.Array<string>, param4: string): globalAndroid.database.Cursor;
            public insert(param0: globalAndroid.net.Uri, param1: globalAndroid.content.ContentValues): globalAndroid.net.Uri;
            public getType(param0: globalAndroid.net.Uri): string;
        }
    }
}


declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module prefs {
                    export module migration {
                        export class MigrationScheme {
                            public static class: java.lang.Class<com.pushwoosh.internal.platform.prefs.migration.MigrationScheme>;
                            public putLong(param0: string, param1: number): void;
                            public put(param0: com.pushwoosh.internal.platform.prefs.PrefsProvider, param1: com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType, param2: string): void;
                            public constructor(param0: string);
                            public putInt(param0: string, param1: number): void;
                            public putBoolean(param0: string, param1: boolean): void;
                            public putString(param0: string, param1: string): void;
                        }
                        export module MigrationScheme {
                            export class AvailableType {
                                public static class: java.lang.Class<com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType>;
                                public static STRING: com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType;
                                public static BOOLEAN: com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType;
                                public static LONG: com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType;
                                public static INT: com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType;
                                public static valueOf(param0: string): com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType;
                                public static values(): native.Array<com.pushwoosh.internal.platform.prefs.migration.MigrationScheme.AvailableType>;
                            }
                        }
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module prefs {
                    export module migration {
                        export class a extends com.pushwoosh.internal.platform.prefs.migration.b {
                            public static class: java.lang.Class<com.pushwoosh.internal.platform.prefs.migration.a>;
                            public a(param0: java.util.Collection<any>): void;
                            public constructor(param0: com.pushwoosh.internal.platform.prefs.PrefsProvider);
                        }
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module prefs {
                    export module migration {
                        export class b {
                            public static class: java.lang.Class<com.pushwoosh.internal.platform.prefs.migration.b>;
                            public constructor(implementation: {
                                a(param0: java.util.Collection<any>): void;
                            });
                            public constructor();
                            public a(param0: java.util.Collection<any>): void;
                        }
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module service {
                    export abstract class PushwooshJobService extends globalAndroid.support.v4.app.FixedPushwooshJobIntentService {
                        public static class: java.lang.Class<com.pushwoosh.internal.platform.service.PushwooshJobService>;
                        public constructor();
                        public static a(param0: java.lang.Class<any>, param1: number, param2: globalAndroid.content.Intent): void;
                        public static a(param0: globalAndroid.content.Context, param1: java.lang.Class<any>, param2: number, param3: globalAndroid.content.Intent): void;
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module utils {
                    export class GeneralUtils {
                        public static class: java.lang.Class<com.pushwoosh.internal.platform.utils.GeneralUtils>;
                        public static SDK_VERSION: string;
                        public static checkNotNull(param0: any, param1: string): void;
                        public static getAppVersion(): number;
                        public static getSenderId(): string;
                        public constructor();
                        public static isStoreApp(): boolean;
                        public static md5(param0: string): string;
                        public static getRawResourses(): java.util.ArrayList<string>;
                        public static parseColor(param0: string): number;
                        public static isNetworkAvailable(): boolean;
                        public static isMainActivity(param0: globalAndroid.app.Activity): boolean;
                        public static checkNotNullOrEmpty(param0: string, param1: string): void;
                        public static checkStickyBroadcastPermissions(param0: globalAndroid.content.Context): boolean;
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module utils {
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module utils {
                    export class d {
                        public static class: java.lang.Class<com.pushwoosh.internal.platform.utils.d>;
                        public run(): void;
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module platform {
                export module utils {
                    export class f {
                        public static class: java.lang.Class<com.pushwoosh.internal.platform.utils.f>;
                        public constructor(implementation: {
                            a(param0: string): void;
                        });
                        public constructor();
                        public a(param0: string): void;
                    }
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceArrayListValue<T> extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceArrayListValue<any>>;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: number);
                    public add(param0: any): void;
                    public get(): java.util.ArrayList<any>;
                    public replaceAll(param0: java.util.Collection<any>): void;
                    public remove(param0: any): void;
                    public clear(): void;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: number, param3: java.lang.Class<any>);
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceBooleanValue extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceBooleanValue>;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: boolean);
                    public set(param0: boolean): void;
                    public get(): boolean;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceClassValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceClassValue>;
                    public get(): java.lang.Class<any>;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: java.lang.Class<any>);
                    public set(param0: java.lang.Class<any>): void;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceIntValue extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceIntValue>;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: number);
                    public set(param0: number): void;
                    public get(): number;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceJsonObjectValue extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceJsonObjectValue>;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string);
                    public set(param0: org.json.JSONObject): void;
                    public get(): org.json.JSONObject;
                    public merge(param0: org.json.JSONObject): void;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceLongValue extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceLongValue>;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: number);
                    public set(param0: number): void;
                    public get(): number;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceSoundTypeValue extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceSoundTypeValue>;
                    public get(): com.pushwoosh.notification.SoundType;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: com.pushwoosh.notification.SoundType);
                    public set(param0: com.pushwoosh.notification.SoundType): void;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceStringValue extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceStringValue>;
                    public get(): string;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: string);
                    public set(param0: string): void;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceValue>;
                    public constructor(implementation: {
                    });
                    public constructor();
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceValueFactory {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceValueFactory>;
                    public buildPreferenceIntValue(param0: globalAndroid.content.SharedPreferences, param1: string, param2: number): com.pushwoosh.internal.preference.PreferenceIntValue;
                    public constructor();
                    public buildPreferenceArrayListValue(param0: globalAndroid.content.SharedPreferences, param1: string, param2: number): com.pushwoosh.internal.preference.PreferenceArrayListValue<string>;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module internal {
            export module preference {
                export class PreferenceVibrateTypeValue extends com.pushwoosh.internal.preference.PreferenceValue {
                    public static class: java.lang.Class<com.pushwoosh.internal.preference.PreferenceVibrateTypeValue>;
                    public constructor(param0: globalAndroid.content.SharedPreferences, param1: string, param2: com.pushwoosh.notification.VibrateType);
                    public set(param0: com.pushwoosh.notification.VibrateType): void;
                    public get(): com.pushwoosh.notification.VibrateType;
                }
            }
        }
    }
}



declare module com {
    export module pushwoosh {
        export module tags {
            export class Tags {
                public static class: java.lang.Class<com.pushwoosh.tags.Tags>;
                public static intTag(param0: string, param1: number): com.pushwoosh.tags.TagsBundle;
                public static empty(): com.pushwoosh.tags.TagsBundle;
                public static stringTag(param0: string, param1: string): com.pushwoosh.tags.TagsBundle;
                public static longTag(param0: string, param1: number): com.pushwoosh.tags.TagsBundle;
                public static appendList(param0: string, param1: java.util.List<string>): com.pushwoosh.tags.TagsBundle;
                public static booleanTag(param0: string, param1: boolean): com.pushwoosh.tags.TagsBundle;
                public static dateTag(param0: string, param1: java.util.Date): com.pushwoosh.tags.TagsBundle;
                public static fromJson(param0: org.json.JSONObject): com.pushwoosh.tags.TagsBundle;
                public static incrementInt(param0: string, param1: number): com.pushwoosh.tags.TagsBundle;
                public static listTag(param0: string, param1: java.util.List<string>): com.pushwoosh.tags.TagsBundle;
                public static removeTag(param0: string): com.pushwoosh.tags.TagsBundle;
            }
            export module Tags {
                export class a {
                    public static class: java.lang.Class<com.pushwoosh.tags.Tags.a>;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module tags {
            export class TagsBundle {
                public static class: java.lang.Class<com.pushwoosh.tags.TagsBundle>;
                public getLong(param0: string, param1: number): number;
                public toJson(): org.json.JSONObject;
                public getBoolean(param0: string, param1: boolean): boolean;
                public getString(param0: string): string;
                public getList(param0: string): java.util.List<string>;
                public getMap(): java.util.Map<string, any>;
                public getInt(param0: string, param1: number): number;
            }
            export module TagsBundle {
                export class Builder {
                    public static class: java.lang.Class<com.pushwoosh.tags.TagsBundle.Builder>;
                    public incrementInt(param0: string, param1: number): com.pushwoosh.tags.TagsBundle.Builder;
                    public putLong(param0: string, param1: number): com.pushwoosh.tags.TagsBundle.Builder;
                    public putInt(param0: string, param1: number): com.pushwoosh.tags.TagsBundle.Builder;
                    public constructor();
                    public putList(param0: string, param1: java.util.List<string>): com.pushwoosh.tags.TagsBundle.Builder;
                    public build(): com.pushwoosh.tags.TagsBundle;
                    public putBoolean(param0: string, param1: boolean): com.pushwoosh.tags.TagsBundle.Builder;
                    public remove(param0: string): com.pushwoosh.tags.TagsBundle.Builder;
                    public appendList(param0: string, param1: java.util.List<string>): com.pushwoosh.tags.TagsBundle.Builder;
                    public putString(param0: string, param1: string): com.pushwoosh.tags.TagsBundle.Builder;
                    public putAll(param0: org.json.JSONObject): com.pushwoosh.tags.TagsBundle.Builder;
                    public putDate(param0: string, param1: java.util.Date): com.pushwoosh.tags.TagsBundle.Builder;
                }
            }
        }
    }
}


declare module com {
    export module pushwoosh {
        export module notification {
            export class Action {
                public static class: java.lang.Class<com.pushwoosh.notification.Action>;
                public getIcon(): string;
                public getExtras(): org.json.JSONObject;
                public constructor(param0: org.json.JSONObject);
                public getIntentAction(): string;
                public getUrl(): string;
                public getActionClass(): java.lang.Class<any>;
                public getType(): com.pushwoosh.notification.Action.a;
                public getTitle(): string;
            }
            export module Action {
                export class a {
                    public static class: java.lang.Class<com.pushwoosh.notification.Action.a>;
                    public static a: com.pushwoosh.notification.Action.a;
                    public static b: com.pushwoosh.notification.Action.a;
                    public static c: com.pushwoosh.notification.Action.a;
                    public static values(): native.Array<com.pushwoosh.notification.Action.a>;
                    public static valueOf(param0: string): com.pushwoosh.notification.Action.a;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class LocalNotification {
                public static class: java.lang.Class<com.pushwoosh.notification.LocalNotification>;
            }
            export module LocalNotification {
                export class Builder {
                    public static class: java.lang.Class<com.pushwoosh.notification.LocalNotification.Builder>;
                    public setTag(param0: string): com.pushwoosh.notification.LocalNotification.Builder;
                    public constructor();
                    public setMessage(param0: string): com.pushwoosh.notification.LocalNotification.Builder;
                    public setDelay(param0: number): com.pushwoosh.notification.LocalNotification.Builder;
                    public build(): com.pushwoosh.notification.LocalNotification;
                    public setLargeIcon(param0: string): com.pushwoosh.notification.LocalNotification.Builder;
                    public setLink(param0: string): com.pushwoosh.notification.LocalNotification.Builder;
                    public setSmallIcon(param0: string): com.pushwoosh.notification.LocalNotification.Builder;
                    public setBanner(param0: string): com.pushwoosh.notification.LocalNotification.Builder;
                    public setExtras(param0: globalAndroid.os.Bundle): com.pushwoosh.notification.LocalNotification.Builder;
                }
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class LocalNotificationReceiver {
                public static class: java.lang.Class<com.pushwoosh.notification.LocalNotificationReceiver>;
                public static TAG: string;
                public static EXTRA_NOTIFICATION_ID: string;
                public static WEEK: number;
                public static rescheduleNotification(param0: any, param1: number): void;
                public static cancelAll(): void;
                public onReceive(param0: globalAndroid.content.Context, param1: globalAndroid.content.Intent): void;
                public static scheduleNotification(param0: globalAndroid.os.Bundle, param1: number): number;
                public static cancelNotification(param0: number): void;
                public constructor();
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class LocalNotificationRequest {
                public static class: java.lang.Class<com.pushwoosh.notification.LocalNotificationRequest>;
                public getRequestId(): number;
                public constructor(param0: number);
                public cancel(): void;
                public unschedule(): void;
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export abstract class NotificationFactory {
                public static class: java.lang.Class<com.pushwoosh.notification.NotificationFactory>;
                public onGenerateNotification(param0: com.pushwoosh.notification.PushMessage): globalAndroid.app.Notification;
                public channelDescription(param0: string): string;
                public addVibration(param0: globalAndroid.app.Notification, param1: boolean): void;
                public getApplicationContext(): globalAndroid.content.Context;
                public addLED(param0: globalAndroid.app.Notification, param1: java.lang.Integer, param2: number, param3: number): void;
                public addCancel(param0: globalAndroid.app.Notification): void;
                public addChannel(param0: com.pushwoosh.notification.PushMessage): string;
                public getContentFromHtml(param0: string): string;
                public getNotificationIntent(param0: com.pushwoosh.notification.PushMessage): globalAndroid.content.Intent;
                public channelName(param0: string): string;
                public addSound(param0: globalAndroid.app.Notification, param1: string): void;
                public constructor();
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class NotificationServiceExtension {
                public static class: java.lang.Class<com.pushwoosh.notification.NotificationServiceExtension>;
                public onMessageReceived(param0: com.pushwoosh.notification.PushMessage): boolean;
                public onMessageOpened(param0: com.pushwoosh.notification.PushMessage): void;
                public getApplicationContext(): globalAndroid.content.Context;
                public handleMessage(param0: globalAndroid.os.Bundle): void;
                public handleNotification(param0: globalAndroid.os.Bundle): void;
                public isAppOnForeground(): boolean;
                public startActivityForPushMessage(param0: com.pushwoosh.notification.PushMessage): void;
                public constructor();
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class PushMessage {
                public static class: java.lang.Class<com.pushwoosh.notification.PushMessage>;
                public isLocal(): boolean;
                public getIconBackgroundColor(): java.lang.Integer;
                public getVibration(): boolean;
                public getCustomData(): string;
                public getBigPictureUrl(): string;
                public getTicker(): string;
                public isLockScreen(): boolean;
                public getPushHash(): string;
                public getLargeIconUrl(): string;
                public getSound(): string;
                public getMessage(): string;
                public getBadges(): number;
                public getActions(): java.util.List<com.pushwoosh.notification.Action>;
                public isSilent(): boolean;
                public getTag(): string;
                public toBundle(): globalAndroid.os.Bundle;
                public getPriority(): number;
                public getLedOffMS(): number;
                public constructor(param0: globalAndroid.os.Bundle);
                public getLedOnMS(): number;
                public getSmallIcon(): number;
                public getHeader(): string;
                public getVisibility(): number;
                public toJson(): org.json.JSONObject;
                public getLed(): java.lang.Integer;
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class PushwooshNotificationFactory extends com.pushwoosh.notification.NotificationFactory {
                public static class: java.lang.Class<com.pushwoosh.notification.PushwooshNotificationFactory>;
                public onGenerateNotification(param0: com.pushwoosh.notification.PushMessage): globalAndroid.app.Notification;
                public getBigPicture(param0: com.pushwoosh.notification.PushMessage): globalAndroid.graphics.Bitmap;
                public getLargeIcon(param0: com.pushwoosh.notification.PushMessage): globalAndroid.graphics.Bitmap;
                public constructor();
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class PushwooshNotificationSettings {
                public static class: java.lang.Class<com.pushwoosh.notification.PushwooshNotificationSettings>;
                public static setMultiNotificationMode(param0: boolean): void;
                public static setNotificationChannelName(param0: string): void;
                public static setSoundNotificationType(param0: com.pushwoosh.notification.SoundType): void;
                public static areNotificationsEnabled(): boolean;
                public static setNotificationIconBackgroundColor(param0: number): void;
                public static enableNotifications(param0: boolean): void;
                public static setColorLED(param0: number): void;
                public static setVibrateNotificationType(param0: com.pushwoosh.notification.VibrateType): void;
                public static setLightScreenOnNotification(param0: boolean): void;
                public static setEnableLED(param0: boolean): void;
                public constructor();
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class SoundType {
                public static class: java.lang.Class<com.pushwoosh.notification.SoundType>;
                public static DEFAULT_MODE: com.pushwoosh.notification.SoundType;
                public static NO_SOUND: com.pushwoosh.notification.SoundType;
                public static ALWAYS: com.pushwoosh.notification.SoundType;
                public static valueOf(param0: string): com.pushwoosh.notification.SoundType;
                public static values(): native.Array<com.pushwoosh.notification.SoundType>;
                public static fromInt(param0: number): com.pushwoosh.notification.SoundType;
                public getValue(): number;
            }
        }
    }
}

declare module com {
    export module pushwoosh {
        export module notification {
            export class VibrateType {
                public static class: java.lang.Class<com.pushwoosh.notification.VibrateType>;
                public static DEFAULT_MODE: com.pushwoosh.notification.VibrateType;
                public static NO_VIBRATE: com.pushwoosh.notification.VibrateType;
                public static ALWAYS: com.pushwoosh.notification.VibrateType;
                public static fromInt(param0: number): com.pushwoosh.notification.VibrateType;
                public static values(): native.Array<com.pushwoosh.notification.VibrateType>;
                public static valueOf(param0: string): com.pushwoosh.notification.VibrateType;
                public getValue(): number;
            }
        }
    }
}
