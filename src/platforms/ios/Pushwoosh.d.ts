
declare class PWAppDelegate extends UIResponder implements UIApplicationDelegate {

	static alloc(): PWAppDelegate; // inherited from NSObject

	static new(): PWAppDelegate; // inherited from NSObject

	readonly debugDescription: string; // inherited from NSObjectProtocol

	readonly description: string; // inherited from NSObjectProtocol

	readonly hash: number; // inherited from NSObjectProtocol

	readonly isProxy: boolean; // inherited from NSObjectProtocol

	readonly superclass: typeof NSObject; // inherited from NSObjectProtocol

	window: UIWindow; // inherited from UIApplicationDelegate

	readonly  // inherited from NSObjectProtocol

	applicationContinueUserActivityRestorationHandler(application: UIApplication, userActivity: NSUserActivity, restorationHandler: (p1: NSArray<UIUserActivityRestoring>) => void): boolean;

	applicationDidBecomeActive(application: UIApplication): void;

	applicationDidChangeStatusBarFrame(application: UIApplication, oldStatusBarFrame: CGRect): void;

	applicationDidChangeStatusBarOrientation(application: UIApplication, oldStatusBarOrientation: UIInterfaceOrientation): void;

	applicationDidDecodeRestorableStateWithCoder(application: UIApplication, coder: NSCoder): void;

	applicationDidEnterBackground(application: UIApplication): void;

	applicationDidFailToContinueUserActivityWithTypeError(application: UIApplication, userActivityType: string, error: NSError): void;

	applicationDidFailToRegisterForRemoteNotificationsWithError(application: UIApplication, error: NSError): void;

	applicationDidFinishLaunching(application: UIApplication): void;

	applicationDidFinishLaunchingWithOptions(application: UIApplication, launchOptions: NSDictionary<any, any>): boolean;

	applicationDidReceiveLocalNotification(application: UIApplication, notification: UILocalNotification): void;

	applicationDidReceiveMemoryWarning(application: UIApplication): void;

	applicationDidReceiveRemoteNotification(application: UIApplication, userInfo: NSDictionary<any, any>): void;

	applicationDidReceiveRemoteNotificationFetchCompletionHandler(application: UIApplication, userInfo: NSDictionary<any, any>, completionHandler: (p1: UIBackgroundFetchResult) => void): void;

	applicationDidRegisterForRemoteNotificationsWithDeviceToken(application: UIApplication, deviceToken: NSData): void;

	applicationDidRegisterUserNotificationSettings(application: UIApplication, notificationSettings: UIUserNotificationSettings): void;

	applicationDidUpdateUserActivity(application: UIApplication, userActivity: NSUserActivity): void;

	applicationHandleActionWithIdentifierForLocalNotificationCompletionHandler(application: UIApplication, identifier: string, notification: UILocalNotification, completionHandler: () => void): void;

	applicationHandleActionWithIdentifierForLocalNotificationWithResponseInfoCompletionHandler(application: UIApplication, identifier: string, notification: UILocalNotification, responseInfo: NSDictionary<any, any>, completionHandler: () => void): void;

	applicationHandleActionWithIdentifierForRemoteNotificationCompletionHandler(application: UIApplication, identifier: string, userInfo: NSDictionary<any, any>, completionHandler: () => void): void;

	applicationHandleActionWithIdentifierForRemoteNotificationWithResponseInfoCompletionHandler(application: UIApplication, identifier: string, userInfo: NSDictionary<any, any>, responseInfo: NSDictionary<any, any>, completionHandler: () => void): void;

	applicationHandleEventsForBackgroundURLSessionCompletionHandler(application: UIApplication, identifier: string, completionHandler: () => void): void;

	applicationHandleIntentCompletionHandler(application: UIApplication, intent: INIntent, completionHandler: (p1: INIntentResponse) => void): void;

	applicationHandleOpenURL(application: UIApplication, url: NSURL): boolean;

	applicationHandleWatchKitExtensionRequestReply(application: UIApplication, userInfo: NSDictionary<any, any>, reply: (p1: NSDictionary<any, any>) => void): void;

	applicationOpenURLOptions(app: UIApplication, url: NSURL, options: NSDictionary<string, any>): boolean;

	applicationOpenURLSourceApplicationAnnotation(application: UIApplication, url: NSURL, sourceApplication: string, annotation: any): boolean;

	applicationPerformActionForShortcutItemCompletionHandler(application: UIApplication, shortcutItem: UIApplicationShortcutItem, completionHandler: (p1: boolean) => void): void;

	applicationPerformFetchWithCompletionHandler(application: UIApplication, completionHandler: (p1: UIBackgroundFetchResult) => void): void;

	applicationProtectedDataDidBecomeAvailable(application: UIApplication): void;

	applicationProtectedDataWillBecomeUnavailable(application: UIApplication): void;

	applicationShouldAllowExtensionPointIdentifier(application: UIApplication, extensionPointIdentifier: string): boolean;

	applicationShouldRequestHealthAuthorization(application: UIApplication): void;

	applicationShouldRestoreApplicationState(application: UIApplication, coder: NSCoder): boolean;

	applicationShouldSaveApplicationState(application: UIApplication, coder: NSCoder): boolean;

	applicationSignificantTimeChange(application: UIApplication): void;

	applicationSupportedInterfaceOrientationsForWindow(application: UIApplication, window: UIWindow): UIInterfaceOrientationMask;

	applicationUserDidAcceptCloudKitShareWithMetadata(application: UIApplication, cloudKitShareMetadata: CKShareMetadata): void;

	applicationViewControllerWithRestorationIdentifierPathCoder(application: UIApplication, identifierComponents: NSArray<string> | string[], coder: NSCoder): UIViewController;

	applicationWillChangeStatusBarFrame(application: UIApplication, newStatusBarFrame: CGRect): void;

	applicationWillChangeStatusBarOrientationDuration(application: UIApplication, newStatusBarOrientation: UIInterfaceOrientation, duration: number): void;

	applicationWillContinueUserActivityWithType(application: UIApplication, userActivityType: string): boolean;

	applicationWillEncodeRestorableStateWithCoder(application: UIApplication, coder: NSCoder): void;

	applicationWillEnterForeground(application: UIApplication): void;

	applicationWillFinishLaunchingWithOptions(application: UIApplication, launchOptions: NSDictionary<any, any>): boolean;

	applicationWillResignActive(application: UIApplication): void;

	applicationWillTerminate(application: UIApplication): void;

	class(): typeof NSObject;

	conformsToProtocol(aProtocol: any /* Protocol */): boolean;

	isEqual(object: any): boolean;

	isKindOfClass(aClass: typeof NSObject): boolean;

	isMemberOfClass(aClass: typeof NSObject): boolean;

	performSelector(aSelector: string): any;

	performSelectorWithObject(aSelector: string, object: any): any;

	performSelectorWithObjectWithObject(aSelector: string, object1: any, object2: any): any;

	respondsToSelector(aSelector: string): boolean;

	retainCount(): number;

	self(): this;
}

declare class PWGDPRManager extends NSObject {

	static alloc(): PWGDPRManager; // inherited from NSObject

	static new(): PWGDPRManager; // inherited from NSObject

	static sharedManager(): PWGDPRManager;

	readonly available: boolean;

	readonly communicationEnabled: boolean;

	readonly deviceDataRemoved: boolean;

	removeAllDeviceDataWithCompletion(completion: (p1: NSError) => void): void;

	setCommunicationEnabledCompletion(enabled: boolean, completion: (p1: NSError) => void): void;

	showGDPRConsentUI(): void;

	showGDPRDeletionUI(): void;
}

declare var PWGDPRStatusDidChangeNotification: string;

declare class PWInAppManager extends NSObject {

	static alloc(): PWInAppManager; // inherited from NSObject

	static new(): PWInAppManager; // inherited from NSObject

	static sharedManager(): PWInAppManager;

	addJavascriptInterfaceWithName(interface: NSObject, name: string): void;

	mergeUserIdToDoMergeCompletion(oldUserId: string, newUserId: string, doMerge: boolean, completion: (p1: NSError) => void): void;

	postEventWithAttributes(event: string, attributes: NSDictionary<any, any>): void;

	postEventWithAttributesCompletion(event: string, attributes: NSDictionary<any, any>, completion: (p1: NSError) => void): void;

	resetBusinessCasesFrequencyCapping(): void;

	setUserId(userId: string): void;
}

declare class PWInbox extends NSObject {

	static addObserverForDidReceiveInPushNotificationCompletion(completion: (p1: NSArray<NSObject>) => void): NSObjectProtocol;

	static addObserverForUpdateInboxMessagesCompletion(completion: (p1: NSArray<string>, p2: NSArray<NSObject>, p3: NSArray<NSObject>) => void): NSObjectProtocol;

	static alloc(): PWInbox; // inherited from NSObject

	static deleteMessagesWithCodes(codes: NSArray<string> | string[]): void;

	static loadMessagesWithCompletion(completion: (p1: NSArray<NSObject>, p2: NSError) => void): void;

	static messagesCountWithCompletion(completion: (p1: number, p2: NSError) => void): void;

	static messagesWithNoActionPerformedCountWithCompletion(completion: (p1: number, p2: NSError) => void): void;

	static new(): PWInbox; // inherited from NSObject

	static performActionForMessageWithCode(code: string): void;

	static readMessagesWithCodes(codes: NSArray<string> | string[]): void;

	static removeObserver(observer: NSObjectProtocol): void;

	static unreadMessagesCountWithCompletion(completion: (p1: number, p2: NSError) => void): void;
}

interface PWInboxMessageProtocol extends NSObjectProtocol {

	code: string;

	imageUrl: string;

	isActionPerformed: boolean;

	isRead: boolean;

	message: string;

	sendDate: Date;

	title: string;

	type: PWInboxMessageType;
}
declare var PWInboxMessageProtocol: {

	prototype: PWInboxMessageProtocol;
};

declare const enum PWInboxMessageType {

	Plain = 0,

	Richmedia = 1,

	URL = 2,

	Deeplink = 3
}

declare var PWInboxMessagesDidReceiveInPushNotification: string;

declare var PWInboxMessagesDidUpdateNotification: string;

declare class PWInlineInAppView extends UIView {

	static alloc(): PWInlineInAppView; // inherited from NSObject

	static appearance(): PWInlineInAppView; // inherited from UIAppearance

	static appearanceForTraitCollection(trait: UITraitCollection): PWInlineInAppView; // inherited from UIAppearance

	static appearanceForTraitCollectionWhenContainedIn(trait: UITraitCollection, ContainerClass: typeof NSObject): PWInlineInAppView; // inherited from UIAppearance

	static appearanceForTraitCollectionWhenContainedInInstancesOfClasses(trait: UITraitCollection, containerTypes: NSArray<typeof NSObject> | typeof NSObject[]): PWInlineInAppView; // inherited from UIAppearance

	static appearanceWhenContainedIn(ContainerClass: typeof NSObject): PWInlineInAppView; // inherited from UIAppearance

	static appearanceWhenContainedInInstancesOfClasses(containerTypes: NSArray<typeof NSObject> | typeof NSObject[]): PWInlineInAppView; // inherited from UIAppearance

	static new(): PWInlineInAppView; // inherited from NSObject

	identifier: string;
}

declare class PWJavaScriptCallback extends NSObject {

	static alloc(): PWJavaScriptCallback; // inherited from NSObject

	static new(): PWJavaScriptCallback; // inherited from NSObject

	execute(): string;

	executeWithParam(param: string): string;

	executeWithParams(params: NSArray<any> | any[]): string;
}

interface PWJavaScriptInterface {

	onWebViewFinishLoad?(webView: UIWebView): void;

	onWebViewStartClose?(webView: UIWebView): void;

	onWebViewStartLoad?(webView: UIWebView): void;
}
declare var PWJavaScriptInterface: {

	prototype: PWJavaScriptInterface;
};

declare class PWLoadingView extends UIView {

	static alloc(): PWLoadingView; // inherited from NSObject

	static appearance(): PWLoadingView; // inherited from UIAppearance

	static appearanceForTraitCollection(trait: UITraitCollection): PWLoadingView; // inherited from UIAppearance

	static appearanceForTraitCollectionWhenContainedIn(trait: UITraitCollection, ContainerClass: typeof NSObject): PWLoadingView; // inherited from UIAppearance

	static appearanceForTraitCollectionWhenContainedInInstancesOfClasses(trait: UITraitCollection, containerTypes: NSArray<typeof NSObject> | typeof NSObject[]): PWLoadingView; // inherited from UIAppearance

	static appearanceWhenContainedIn(ContainerClass: typeof NSObject): PWLoadingView; // inherited from UIAppearance

	static appearanceWhenContainedInInstancesOfClasses(containerTypes: NSArray<typeof NSObject> | typeof NSObject[]): PWLoadingView; // inherited from UIAppearance

	static new(): PWLoadingView; // inherited from NSObject

	activityIndicatorView: UIActivityIndicatorView;

	cancelLoadingButton: UIButton;
}

declare class PWLog extends NSObject {

	static alloc(): PWLog; // inherited from NSObject

	static new(): PWLog; // inherited from NSObject

	static removeLogsHandler(): void;

	static setLogsHandler(logsHandler: (p1: string, p2: string) => void): void;
}

declare var PWLogLevelDebug: string;

declare var PWLogLevelError: string;

declare var PWLogLevelInfo: string;

declare var PWLogLevelNone: string;

declare var PWLogLevelVerbose: string;

declare var PWLogLevelWarning: string;

declare class PWRichMedia extends NSObject {

	static alloc(): PWRichMedia; // inherited from NSObject

	static new(): PWRichMedia; // inherited from NSObject

	readonly content: string;

	readonly required: boolean;

	readonly source: PWRichMediaSource;
}

declare class PWRichMediaManager extends NSObject {

	static alloc(): PWRichMediaManager; // inherited from NSObject

	static new(): PWRichMediaManager; // inherited from NSObject

	static sharedManager(): PWRichMediaManager;

	delegate: PWRichMediaPresentingDelegate;

	richMediaStyle: PWRichMediaStyle;

	presentRichMedia(richMedia: PWRichMedia): void;
}

interface PWRichMediaPresentingDelegate extends NSObjectProtocol {

	richMediaManagerDidCloseRichMedia?(richMediaManager: PWRichMediaManager, richMedia: PWRichMedia): void;

	richMediaManagerDidPresentRichMedia?(richMediaManager: PWRichMediaManager, richMedia: PWRichMedia): void;

	richMediaManagerPresentingDidFailForRichMediaWithError?(richMediaManager: PWRichMediaManager, richMedia: PWRichMedia, error: NSError): void;

	richMediaManagerShouldPresentRichMedia?(richMediaManager: PWRichMediaManager, richMedia: PWRichMedia): boolean;
}
declare var PWRichMediaPresentingDelegate: {

	prototype: PWRichMediaPresentingDelegate;
};

declare const enum PWRichMediaSource {

	Push = 0,

	InApp = 1
}

declare class PWRichMediaStyle extends NSObject {

	static alloc(): PWRichMediaStyle; // inherited from NSObject

	static new(): PWRichMediaStyle; // inherited from NSObject

	allowsInlineMediaPlayback: number;

	animationDelegate: PWRichMediaStyleAnimationDelegate;

	backgroundColor: UIColor;

	closeButtonPresentingDelay: number;

	loadingViewBlock: () => PWLoadingView;

	mediaPlaybackRequiresUserAction: number;

	shouldHideStatusBar: boolean;
}

interface PWRichMediaStyleAnimationDelegate extends NSObjectProtocol {

	runDismissingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	runPresentingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;
}
declare var PWRichMediaStyleAnimationDelegate: {

	prototype: PWRichMediaStyleAnimationDelegate;
};

declare class PWRichMediaStyleCrossFadeAnimation extends NSObject implements PWRichMediaStyleAnimationDelegate {

	static alloc(): PWRichMediaStyleCrossFadeAnimation; // inherited from NSObject

	static new(): PWRichMediaStyleCrossFadeAnimation; // inherited from NSObject

	readonly debugDescription: string; // inherited from NSObjectProtocol

	readonly description: string; // inherited from NSObjectProtocol

	readonly hash: number; // inherited from NSObjectProtocol

	readonly isProxy: boolean; // inherited from NSObjectProtocol

	readonly superclass: typeof NSObject; // inherited from NSObjectProtocol

	readonly  // inherited from NSObjectProtocol

	class(): typeof NSObject;

	conformsToProtocol(aProtocol: any /* Protocol */): boolean;

	isEqual(object: any): boolean;

	isKindOfClass(aClass: typeof NSObject): boolean;

	isMemberOfClass(aClass: typeof NSObject): boolean;

	performSelector(aSelector: string): any;

	performSelectorWithObject(aSelector: string, object: any): any;

	performSelectorWithObjectWithObject(aSelector: string, object1: any, object2: any): any;

	respondsToSelector(aSelector: string): boolean;

	retainCount(): number;

	runDismissingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	runPresentingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	self(): this;
}

declare var PWRichMediaStyleDefaultAnimationDuration: number;

declare class PWRichMediaStyleSlideBottomAnimation extends NSObject implements PWRichMediaStyleAnimationDelegate {

	static alloc(): PWRichMediaStyleSlideBottomAnimation; // inherited from NSObject

	static new(): PWRichMediaStyleSlideBottomAnimation; // inherited from NSObject

	readonly debugDescription: string; // inherited from NSObjectProtocol

	readonly description: string; // inherited from NSObjectProtocol

	readonly hash: number; // inherited from NSObjectProtocol

	readonly isProxy: boolean; // inherited from NSObjectProtocol

	readonly superclass: typeof NSObject; // inherited from NSObjectProtocol

	readonly  // inherited from NSObjectProtocol

	class(): typeof NSObject;

	conformsToProtocol(aProtocol: any /* Protocol */): boolean;

	isEqual(object: any): boolean;

	isKindOfClass(aClass: typeof NSObject): boolean;

	isMemberOfClass(aClass: typeof NSObject): boolean;

	performSelector(aSelector: string): any;

	performSelectorWithObject(aSelector: string, object: any): any;

	performSelectorWithObjectWithObject(aSelector: string, object1: any, object2: any): any;

	respondsToSelector(aSelector: string): boolean;

	retainCount(): number;

	runDismissingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	runPresentingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	self(): this;
}

declare class PWRichMediaStyleSlideLeftAnimation extends NSObject implements PWRichMediaStyleAnimationDelegate {

	static alloc(): PWRichMediaStyleSlideLeftAnimation; // inherited from NSObject

	static new(): PWRichMediaStyleSlideLeftAnimation; // inherited from NSObject

	readonly debugDescription: string; // inherited from NSObjectProtocol

	readonly description: string; // inherited from NSObjectProtocol

	readonly hash: number; // inherited from NSObjectProtocol

	readonly isProxy: boolean; // inherited from NSObjectProtocol

	readonly superclass: typeof NSObject; // inherited from NSObjectProtocol

	readonly  // inherited from NSObjectProtocol

	class(): typeof NSObject;

	conformsToProtocol(aProtocol: any /* Protocol */): boolean;

	isEqual(object: any): boolean;

	isKindOfClass(aClass: typeof NSObject): boolean;

	isMemberOfClass(aClass: typeof NSObject): boolean;

	performSelector(aSelector: string): any;

	performSelectorWithObject(aSelector: string, object: any): any;

	performSelectorWithObjectWithObject(aSelector: string, object1: any, object2: any): any;

	respondsToSelector(aSelector: string): boolean;

	retainCount(): number;

	runDismissingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	runPresentingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	self(): this;
}

declare class PWRichMediaStyleSlideRightAnimation extends NSObject implements PWRichMediaStyleAnimationDelegate {

	static alloc(): PWRichMediaStyleSlideRightAnimation; // inherited from NSObject

	static new(): PWRichMediaStyleSlideRightAnimation; // inherited from NSObject

	readonly debugDescription: string; // inherited from NSObjectProtocol

	readonly description: string; // inherited from NSObjectProtocol

	readonly hash: number; // inherited from NSObjectProtocol

	readonly isProxy: boolean; // inherited from NSObjectProtocol

	readonly superclass: typeof NSObject; // inherited from NSObjectProtocol

	readonly  // inherited from NSObjectProtocol

	class(): typeof NSObject;

	conformsToProtocol(aProtocol: any /* Protocol */): boolean;

	isEqual(object: any): boolean;

	isKindOfClass(aClass: typeof NSObject): boolean;

	isMemberOfClass(aClass: typeof NSObject): boolean;

	performSelector(aSelector: string): any;

	performSelectorWithObject(aSelector: string, object: any): any;

	performSelectorWithObjectWithObject(aSelector: string, object1: any, object2: any): any;

	respondsToSelector(aSelector: string): boolean;

	retainCount(): number;

	runDismissingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	runPresentingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	self(): this;
}

declare class PWRichMediaStyleSlideTopAnimation extends NSObject implements PWRichMediaStyleAnimationDelegate {

	static alloc(): PWRichMediaStyleSlideTopAnimation; // inherited from NSObject

	static new(): PWRichMediaStyleSlideTopAnimation; // inherited from NSObject

	readonly debugDescription: string; // inherited from NSObjectProtocol

	readonly description: string; // inherited from NSObjectProtocol

	readonly hash: number; // inherited from NSObjectProtocol

	readonly isProxy: boolean; // inherited from NSObjectProtocol

	readonly superclass: typeof NSObject; // inherited from NSObjectProtocol

	readonly  // inherited from NSObjectProtocol

	class(): typeof NSObject;

	conformsToProtocol(aProtocol: any /* Protocol */): boolean;

	isEqual(object: any): boolean;

	isKindOfClass(aClass: typeof NSObject): boolean;

	isMemberOfClass(aClass: typeof NSObject): boolean;

	performSelector(aSelector: string): any;

	performSelectorWithObject(aSelector: string, object: any): any;

	performSelectorWithObjectWithObject(aSelector: string, object1: any, object2: any): any;

	respondsToSelector(aSelector: string): boolean;

	retainCount(): number;

	runDismissingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	runPresentingAnimationWithContentViewParentViewCompletion(contentView: UIView, parentView: UIView, completion: () => void): void;

	self(): this;
}

declare class PWTags extends NSObject {

	static alloc(): PWTags; // inherited from NSObject

	static incrementalTagWithInteger(delta: number): NSDictionary<any, any>;

	static new(): PWTags; // inherited from NSObject
}

interface PushNotificationDelegate {

	onDidFailToRegisterForRemoteNotificationsWithError?(error: NSError): void;

	onDidRegisterForRemoteNotificationsWithDeviceToken?(token: string): void;

	onInAppClosed?(code: string): void;

	onInAppDisplayed?(code: string): void;

	onPushAcceptedWithNotification?(pushManager: PushNotificationManager, pushNotification: NSDictionary<any, any>): void;

	onPushAcceptedWithNotificationOnStart?(pushManager: PushNotificationManager, pushNotification: NSDictionary<any, any>, onStart: boolean): void;

	onPushReceivedWithNotificationOnStart?(pushManager: PushNotificationManager, pushNotification: NSDictionary<any, any>, onStart: boolean): void;

	onTagsFailedToReceive?(error: NSError): void;

	onTagsReceived?(tags: NSDictionary<any, any>): void;

	pushManagerOpenSettingsForNotification?(pushManager: PushNotificationManager, notification: UNNotification): void;
}
declare var PushNotificationDelegate: {

	prototype: PushNotificationDelegate;
};

declare class PushNotificationManager extends NSObject {

	static alloc(): PushNotificationManager; // inherited from NSObject

	static clearNotificationCenter(): void;

	static getRemoteNotificationStatus(): NSMutableDictionary<any, any>;

	static initializeWithAppCodeAppName(appCode: string, appName: string): void;

	static new(): PushNotificationManager; // inherited from NSObject

	static pushManager(): PushNotificationManager;

	static pushwooshVersion(): string;

	additionalAuthorizationOptions: UNAuthorizationOptions;

	readonly appCode: string;

	readonly appName: string;

	delegate: NSObject;

	readonly launchNotification: NSDictionary<any, any>;

	readonly notificationCenterDelegate: UNUserNotificationCenterDelegate;

	showPushnotificationAlert: boolean;

	constructor(o: { applicationCode: string; appName: string; });

	constructor(o: { applicationCode: string; navController: UIViewController; appName: string; });

	getApnPayload(pushNotification: NSDictionary<any, any>): NSDictionary<any, any>;

	getCustomPushData(pushNotification: NSDictionary<any, any>): string;

	getCustomPushDataAsNSDict(pushNotification: NSDictionary<any, any>): NSDictionary<any, any>;

	getHWID(): string;

	getPushToken(): string;

	handlePushReceived(userInfo: NSDictionary<any, any>): boolean;

	handlePushRegistration(devToken: NSData): void;

	handlePushRegistrationFailure(error: NSError): void;

	handlePushRegistrationString(deviceID: string): void;

	initWithApplicationCodeAppName(appCode: string, appName: string): this;

	initWithApplicationCodeNavControllerAppName(appCode: string, navController: UIViewController, appName: string): this;

	loadTags(): void;

	loadTagsError(successHandler: (p1: NSDictionary<any, any>) => void, errorHandler: (p1: NSError) => void): void;

	mergeUserIdToDoMergeCompletion(oldUserId: string, newUserId: string, doMerge: boolean, completion: (p1: NSError) => void): void;

	postEventWithAttributes(event: string, attributes: NSDictionary<any, any>): void;

	postEventWithAttributesCompletion(event: string, attributes: NSDictionary<any, any>, completion: (p1: NSError) => void): void;

	registerForPushNotifications(): void;

	sendAppOpen(): void;

	sendBadges(badge: number): void;

	sendLocation(location: CLLocation): void;

	sendPurchaseWithPriceCurrencyCodeAndDate(productIdentifier: string, price: NSDecimalNumber, currencyCode: string, date: Date): void;

	sendSKPaymentTransactions(transactions: NSArray<any> | any[]): void;

	setTags(tags: NSDictionary<any, any>): void;

	setTagsWithCompletion(tags: NSDictionary<any, any>, completion: (p1: NSError) => void): void;

	setUserId(userId: string): void;

	startLocationTracking(): void;

	stopLocationTracking(): void;

	unregisterForPushNotifications(): void;

	unregisterForPushNotificationsWithCompletion(completion: (p1: NSError) => void): void;
}
