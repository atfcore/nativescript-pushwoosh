import { Common } from './pushwoosh.common';
export declare class Pushwoosh extends Common {
    static getInstance(): typeof PushwooshAppDelegate;
}
declare class PushwooshAppDelegate extends UIResponder implements UIApplicationDelegate {
    static ObjCProtocols: {
        prototype: UIApplicationDelegate;
    }[];
    static ObjCExposedMethods: {
        "runOnBackground": {
            returns: interop.Type<void>;
        };
    };
    applicationDidFinishLaunchingWithOptions(application: UIApplication, launchOptions: NSDictionary<string, any>): boolean;
    applicationDidRegisterForRemoteNotificationsWithDeviceToken(application: UIApplication, devToken: NSData): void;
    applicationDidFailToRegisterForRemoteNotificationsWithError(application: UIApplication, error: NSError): void;
    applicationDidReceiveRemoteNotificationFetchCompletionHandler(application: UIApplication, userInfo: NSDictionary<any, any>, completionHandler: (p1: UIBackgroundFetchResult) => void): void;
    onPushReceivedWithNotificationOnStart(pushManager: PushNotificationManager, pushNotification: NSDictionary<any, any>, onStart: boolean): void;
}
export {};
