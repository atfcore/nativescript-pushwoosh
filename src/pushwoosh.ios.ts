/// <reference path="./node_modules/tns-platform-declarations/ios.d.ts" />

import { Common } from './pushwoosh.common';

export class Pushwoosh extends Common {
    static getInstance() {
        return PushwooshAppDelegate;
    }
}

class PushwooshAppDelegate extends UIResponder implements UIApplicationDelegate {
    public static ObjCProtocols = [UIApplicationDelegate];
    public static ObjCExposedMethods = {
        "runOnBackground": { returns: interop.types.void }
    };
    
    public applicationDidFinishLaunchingWithOptions(application: UIApplication, launchOptions: NSDictionary<string, any>): boolean {
        let pushManager = PushNotificationManager.pushManager();
        pushManager.delegate = this;

        UNUserNotificationCenter.currentNotificationCenter().delegate = pushManager.notificationCenterDelegate;

        pushManager.sendAppOpen();

        pushManager.registerForPushNotifications();

        return true;
    }

    public applicationDidRegisterForRemoteNotificationsWithDeviceToken(application: UIApplication, devToken: NSData) {
        // console.log(" applicationDidRegisterForRemoteNotificationsWithDeviceToken - deviceToken: " + devToken);
        let pushManager = PushNotificationManager.pushManager();
        pushManager.handlePushRegistration(devToken);
    }

    public applicationDidFailToRegisterForRemoteNotificationsWithError(application: UIApplication, error: NSError) {
        // if(!!error) {
        //     console.log(" Errore in applicationDidFailToRegisterForRemoteNotificationsWithError: ")
        //     console.log(error);
        // }
        let pushManager = PushNotificationManager.pushManager();
        pushManager.handlePushRegistrationFailure(error);
    }

    public applicationDidReceiveRemoteNotificationFetchCompletionHandler(application: UIApplication, userInfo: NSDictionary<any, any>, completionHandler: (p1: UIBackgroundFetchResult) => void) {
        PushNotificationManager.pushManager().handlePushReceived(userInfo);
        completionHandler(UIBackgroundFetchResult.NoData);
    }

    public onPushReceivedWithNotificationOnStart(pushManager: PushNotificationManager, pushNotification: NSDictionary<any,any>, onStart: boolean) {
        // console.log("NOTIFICATION RECEIVED");
    }

    /* public onPushAcceptedWithNotificationOnStart() {
        
    } */

}