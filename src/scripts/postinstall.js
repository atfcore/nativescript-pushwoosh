var path = require("path");
var fs = require("fs");

// in caso di installazione da node-modules
const appRoot = "../../";
const externalPushClientOnly = true;

console.log(" # dirname: " + __dirname)

writeGoogleServiceCopyHook();

/**
 * Installs an after-prepare build hook to copy the app/App_Resources/Android/google-services.json to platform/android on build.
 * Installs before-checkForChange build hook to detect changes in environment and copy GoogleServices.plist on build.
 */
function writeGoogleServiceCopyHook() {
    // Install after-prepare hook
    console.log("Install google-service.json after-prepare copy hook.");
    try {
        var afterPrepareScriptContent =
            `
  var path = require("path");
  var fs = require("fs");
  module.exports = function($logger, $projectData, hookArgs) {
  return new Promise(function(resolve, reject) {
          /* Decide whether to prepare for dev or prod environment */
          var isReleaseBuild = (hookArgs.appFilesUpdaterOptions || hookArgs.prepareData).release;
          var validProdEnvs = ['prod','production'];
          var isProdEnv = false; // building with --env.prod or --env.production flag
          var env = (hookArgs.platformSpecificData || hookArgs.prepareData).env;
          if (env) {
              Object.keys(env).forEach((key) => {
                  if (validProdEnvs.indexOf(key)>-1) { isProdEnv=true; }
              });
          }
          var buildType = isReleaseBuild || isProdEnv ? 'production' : 'development';
          const platformFromHookArgs = hookArgs && (hookArgs.platform || (hookArgs.prepareData && hookArgs.prepareData.platform));
          const platform = (platformFromHookArgs  || '').toLowerCase();
          /* Create info file in platforms dir so we can detect changes in environment and force prepare if needed */
          var npfInfoPath = path.join($projectData.platformsDir, platform, ".pluginfirebaseinfo");
          var npfInfo = {
              buildType: buildType,
          };
          try { fs.writeFileSync(npfInfoPath, JSON.stringify(npfInfo)); }
          catch (err) {
              $logger.info('nativescript-plugin-firebase: unable to create '+npfInfoPath+', prepare will be forced next time!');
          }
          /* Handle preparing of Google Services files */
          if (platform === 'android') {
              var destinationGoogleJson = path.join($projectData.platformsDir, "android", "app", "google-services.json");
              var destinationGoogleJsonAlt = path.join($projectData.platformsDir, "android", "google-services.json");
              var sourceGoogleJson = path.join($projectData.appResourcesDirectoryPath, "Android", "google-services.json");
              var sourceGoogleJsonProd = path.join($projectData.appResourcesDirectoryPath, "Android", "google-services.json.prod");
              var sourceGoogleJsonDev = path.join($projectData.appResourcesDirectoryPath, "Android", "google-services.json.dev");
              // ensure we have both dev/prod versions so we never overwrite singlular google-services.json
              if (fs.existsSync(sourceGoogleJsonProd) && fs.existsSync(sourceGoogleJsonDev)) {
                  if (buildType==='production') { sourceGoogleJson = sourceGoogleJsonProd; } // use prod version
                  else { sourceGoogleJson = sourceGoogleJsonDev; } // use dev version
              }
              // copy correct version to destination
              if (fs.existsSync(sourceGoogleJson) && fs.existsSync(path.dirname(destinationGoogleJson))) {
                  $logger.info("Copy " + sourceGoogleJson + " to " + destinationGoogleJson + ".");
                  fs.writeFileSync(destinationGoogleJson, fs.readFileSync(sourceGoogleJson));
                  resolve();
              } else if (fs.existsSync(sourceGoogleJson) && fs.existsSync(path.dirname(destinationGoogleJsonAlt))) {
                  // NativeScript < 4 doesn't have the 'app' folder
                  $logger.info("Copy " + sourceGoogleJson + " to " + destinationGoogleJsonAlt + ".");
                  fs.writeFileSync(destinationGoogleJsonAlt, fs.readFileSync(sourceGoogleJson));
                  resolve();
              } else {
                  $logger.warn("Unable to copy google-services.json.");
                  reject();
              }
          } else if (platform === 'ios') {
              // we have copied our GoogleService-Info.plist during before-checkForChanges hook, here we delete it to avoid changes in git
              var destinationGooglePlist = path.join($projectData.appResourcesDirectoryPath, "iOS", "GoogleService-Info.plist");
              var sourceGooglePlistProd = path.join($projectData.appResourcesDirectoryPath, "iOS", "GoogleService-Info.plist.prod");
              var sourceGooglePlistDev = path.join($projectData.appResourcesDirectoryPath, "iOS", "GoogleService-Info.plist.dev");
              // if we have both dev/prod versions, let's remove GoogleService-Info.plist in destination dir
              if (fs.existsSync(sourceGooglePlistProd) && fs.existsSync(sourceGooglePlistDev)) {
                  if (fs.existsSync(destinationGooglePlist)) { fs.unlinkSync(destinationGooglePlist); }
                  resolve();
              } else { // single GoogleService-Info.plist modus
                  resolve();
              }
          } else {
              resolve();
          }
      });
  };
  `;
        var scriptPath = path.join(appRoot, "hooks", "after-prepare", "firebase-copy-google-services.js");
        var afterPrepareDirPath = path.dirname(scriptPath);
        var hooksDirPath = path.dirname(afterPrepareDirPath);
        if (!fs.existsSync(afterPrepareDirPath)) {
            if (!fs.existsSync(hooksDirPath)) {
                fs.mkdirSync(hooksDirPath);
            }
            fs.mkdirSync(afterPrepareDirPath);
        }
        fs.writeFileSync(scriptPath, afterPrepareScriptContent);
    } catch (e) {
        console.log("Failed to install google-service.json after-prepare copy hook.");
        console.log(e);
    }

    /*
       Install before-checkForChanges hook
    */

    console.log("Install google-service.json before-checkForChanges copy hook.");
    try {
        var beforeCheckForChangesContent =
            `
  var path = require("path");
  var fs = require("fs");
  module.exports = function($logger, hookArgs) {
      return new Promise(function(resolve, reject) {
          /* Decide whether to prepare for dev or prod environment */
          var isReleaseBuild = !!((hookArgs.checkForChangesOpts && hookArgs.checkForChangesOpts.projectChangesOptions) || hookArgs.prepareData).release;
          var validProdEnvs = ['prod','production'];
          var isProdEnv = false; // building with --env.prod or --env.production flag
          var env = ((hookArgs.checkForChangesOpts && hookArgs.checkForChangesOpts.projectData && hookArgs.checkForChangesOpts.projectData.$options && hookArgs.checkForChangesOpts.projectData.$options.argv) || hookArgs.prepareData).env;
          if (env) {
              Object.keys(env).forEach((key) => {
                  if (validProdEnvs.indexOf(key)>-1) { isProdEnv=true; }
              });
          }
          var buildType = isReleaseBuild || isProdEnv ? 'production' : 'development';
          /*
              Detect if we have nativescript-plugin-firebase temp file created during after-prepare hook, so we know
              for which environment {development|prod} the project was prepared. If needed, we delete the NS .nsprepareinfo
              file so we force a new prepare
          */
          var platform = (hookArgs.checkForChangesOpts || hookArgs.prepareData).platform.toLowerCase();
          var projectData = (hookArgs.checkForChangesOpts && hookArgs.checkForChangesOpts.projectData) || hookArgs.projectData;
          var platformsDir = projectData.platformsDir;
          var appResourcesDirectoryPath = projectData.appResourcesDirectoryPath;
          var forcePrepare = true; // whether to force NS to run prepare, defaults to true
          var npfInfoPath = path.join(platformsDir, platform, ".pluginfirebaseinfo");
          var nsPrepareInfoPath = path.join(platformsDir, platform, ".nsprepareinfo");
          var copyPlistOpts = { platform, appResourcesDirectoryPath, buildType, $logger }
          if (fs.existsSync(npfInfoPath)) {
              var npfInfo = undefined;
              try { npfInfo = JSON.parse(fs.readFileSync(npfInfoPath, 'utf8')); }
              catch (e) { $logger.info('nativescript-plugin-firebase: error reading '+npfInfoPath); }
              if (npfInfo && npfInfo.hasOwnProperty('buildType') && npfInfo.buildType===buildType) {
                  $logger.info('nativescript-plugin-firebase: building for same environment, not forcing prepare.');
                  forcePrepare=false;
              }
          } else { $logger.info('nativescript-plugin-firebase: '+npfInfoPath+' not found, forcing prepare!'); }
          if (forcePrepare) {
              $logger.info('nativescript-plugin-firebase: running release build or change in environment detected, forcing prepare!');
              if (fs.existsSync(npfInfoPath)) { fs.unlinkSync(npfInfoPath); }
              if (fs.existsSync(nsPrepareInfoPath)) { fs.unlinkSync(nsPrepareInfoPath); }
              if (copyPlist(copyPlistOpts)) { resolve(); } else { reject(); }
          } else { resolve(); }
      });
  };
  /*
      Handle preparing of Google Services files for iOS
  */
  var copyPlist = function(copyPlistOpts) {
      if (copyPlistOpts.platform === 'android') { return true; }
      else if (copyPlistOpts.platform === 'ios') {
          var sourceGooglePlistProd = path.join(copyPlistOpts.appResourcesDirectoryPath, "iOS", "GoogleService-Info.plist.prod");
          var sourceGooglePlistDev = path.join(copyPlistOpts.appResourcesDirectoryPath, "iOS", "GoogleService-Info.plist.dev");
          var destinationGooglePlist = path.join(copyPlistOpts.appResourcesDirectoryPath, "iOS", "GoogleService-Info.plist");
          // if we have both dev/prod versions, we copy (or overwrite) GoogleService-Info.plist in destination dir
          if (fs.existsSync(sourceGooglePlistProd) && fs.existsSync(sourceGooglePlistDev)) {
              if (copyPlistOpts.buildType==='production') { // use prod version
                  copyPlistOpts.$logger.info("nativescript-plugin-firebase: copy " + sourceGooglePlistProd + " to " + destinationGooglePlist + ".");
                  fs.writeFileSync(destinationGooglePlist, fs.readFileSync(sourceGooglePlistProd));
                  return true;
              } else { // use dev version
                  copyPlistOpts.$logger.info("nativescript-plugin-firebase: copy " + sourceGooglePlistDev + " to " + destinationGooglePlist + ".");
                  fs.writeFileSync(destinationGooglePlist, fs.readFileSync(sourceGooglePlistDev));
                  return true;
              }
          } else if (!fs.existsSync(destinationGooglePlist)) { // single GoogleService-Info.plist modus but missing`;
        if (externalPushClientOnly) {
            beforeCheckForChangesContent += `
              return true; // this is a push-only project, so this is allowed`;
        } else {
            beforeCheckForChangesContent += `
              copyPlistOpts.$logger.warn("nativescript-plugin-firebase: " + destinationGooglePlist + " does not exist. Please follow the installation instructions from the documentation");
              return false;`;
        }
        beforeCheckForChangesContent += `
          } else {
              return true; // single GoogleService-Info.plist modus
          }
      } else { return true; }
  }
  `;
        var scriptPath = path.join(appRoot, "hooks", "before-checkForChanges", "firebase-copy-google-services.js");
        var afterPrepareDirPath = path.dirname(scriptPath);
        var hooksDirPath = path.dirname(afterPrepareDirPath);
        if (!fs.existsSync(afterPrepareDirPath)) {
            if (!fs.existsSync(hooksDirPath)) {
                fs.mkdirSync(hooksDirPath);
            }
            fs.mkdirSync(afterPrepareDirPath);
        }
        fs.writeFileSync(scriptPath, beforeCheckForChangesContent);
    } catch (e) {
        console.log("Failed to install google-service.json before-checkForChanges copy hook.");
        console.log(e);
    }
}