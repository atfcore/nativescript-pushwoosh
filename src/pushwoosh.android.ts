/// <reference path="./platforms/android/Pushwoosh.d.ts"/>

import { Common } from './pushwoosh.common';

export class Pushwoosh extends Common {
    static getInstance() {
        // console.log(" ### invoking native API com.pushwoosh.Pushwoosh.getInstance()")
        return com.pushwoosh.Pushwoosh.getInstance();
    }
}
